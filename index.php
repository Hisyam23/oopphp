<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("sheep");

echo "Name: $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blooded : $sheep->cold_blooded <br><br>";

$kodok = new Frog("buduk");

echo "Name: $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
$kodok->jump();
echo "<br><br>";

$ape = new Ape("Kera Sakti");

echo "Name: $ape->name <br>";
echo "Legs : $ape->legs <br>";
echo "Cold Blooded : $ape->cold_blooded <br>";
$ape->yell();